def add (a, b) :
    res = a + b
    return res

def mul(a, b) :
    res = 0
    for i in range(a) :
        res = add(res, b)
    return res

def square(a) :
    res = mul(a, a)
    return res

def dans(liste, elem) -> bool :
    if liste == [] :
        return False
    if elem == liste[0] :
        return True
    return dans(liste[1:], elem)

def factorielle(n) :
    res = 1
    if n > 1 :
        res = n * factorielle(n-1)
    return res

def factorielle_alt(n) :
    res = 1
    if n > 1 :
         prec = factorielle_alt(n-1)
         res = n * prec
    return res

def palindrome_alt(s) :
    if s == "" :
        return True
    if s[0] !=  s[-1] :
        return False
    return palindrome(s[1:-1])

def palindrome(s) :
    if s == "" :
        return True
    premier = s[0]
    dernier = s[-1]
    if premier !=  dernier :
        return False
    centre = s[1:-1]
    return palindrome(centre)

def crepier(pile) :
    if nombre_crepes(pile) <= 1 :
        return pile
    retourner(pile, PLUS_GRANDE)
    retourner(pile, PLUS_BASSE)
    max, haut = pile[0], pile[1:]
    ordonnee = crepier(haut)
    return max + ordonnee
